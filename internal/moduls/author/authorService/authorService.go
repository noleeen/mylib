package authorService

import (
	"context"
	"database/sql"
	"gitlab.com/mylib/internal/model"
	"gitlab.com/mylib/internal/repository/postgres/authorStorage"
)

type AuthorService struct {
	authorStorage authorStorage.AuthorStorager
}

func NewAuthorService(db *sql.DB) AuthorServicer {
	return &AuthorService{authorStorage: authorStorage.NewAuthorStorage(db)}
}

func (a *AuthorService) AddAuthor(ctx context.Context, name string) (model.Author, error) {
	newAuthor := model.Author{Name: name}
	author, err := a.authorStorage.AddAuthor(ctx, newAuthor)
	if err != nil {
		return model.Author{}, err
	}
	return author, nil
}
func (a *AuthorService) ListAuthors(ctx context.Context) ([]model.Author, error) {
	authors, err := a.authorStorage.ListsAuthor(ctx)
	return authors, err
}
func (a *AuthorService) TopReadAuthors(ctx context.Context, limit int) ([]model.Author, error) {
	authors, err := a.authorStorage.TopReadingAuthors(ctx, limit)
	return authors, err
}
func (a *AuthorService) TopAuthorsInTheHistory(ctx context.Context, limit int) ([]model.Author, error) {
	authors, err := a.authorStorage.TopAuthorsInTheHistory(ctx, limit)
	return authors, err
}
func (a *AuthorService) AddReadCount(ctx context.Context, authorId int) (model.Author, error) {
	authors, err := a.authorStorage.AddReadCount(ctx, authorId)
	return authors, err
}
