package authorService

import (
	"context"
	"gitlab.com/mylib/internal/model"
)

type AuthorServicer interface {
	AddAuthor(ctx context.Context, name string) (model.Author, error)
	ListAuthors(ctx context.Context) ([]model.Author, error)
	TopReadAuthors(ctx context.Context, limit int) ([]model.Author, error)
	TopAuthorsInTheHistory(ctx context.Context, limit int) ([]model.Author, error)
	AddReadCount(ctx context.Context, authorId int) (model.Author, error)
}
