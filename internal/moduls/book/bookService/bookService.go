package bookService

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/mylib/internal/model"
	"gitlab.com/mylib/internal/repository/postgres/bookStorage"
	"sync"
)

type BookService struct {
	bookStorage bookStorage.BookStorager
	sync.Mutex
}

func NewBookService(db *sql.DB) BookServicer {
	return &BookService{bookStorage: bookStorage.NewBookStorage(db)}
}

func (b *BookService) AddBook(ctx context.Context, title string, authorID int) (model.Book, error) {
	book := model.Book{
		Title:    title,
		AuthorId: authorID,
	}

	addBook, err := b.bookStorage.AddBook(ctx, book)
	return addBook, err

}

func (b *BookService) ListBooks(ctx context.Context) ([]model.Book, error) {
	books, err := b.bookStorage.ListBooks(ctx)

	return books, err
}

func (b *BookService) RentBook(ctx context.Context, bookId, userId int) (model.Book, error) {
	b.Mutex.Lock()
	defer b.Unlock()
	book, err := b.bookStorage.GetBook(ctx, bookId)
	if err != nil {
		return model.Book{}, err
	}
	if book.RentedUserId != 0 {
		return model.Book{}, fmt.Errorf("book with id %d already rented", bookId)
	}

	book.RentedUserId = userId

	if err = b.bookStorage.UpdateBook(ctx, book); err != nil {
		return model.Book{}, err
	}

	return book, nil
}

func (b *BookService) ReturnBook(ctx context.Context, bookId, userId int) (model.Book, error) {
	b.Mutex.Lock()
	defer b.Unlock()

	book, err := b.bookStorage.GetBook(ctx, bookId)
	if err != nil {
		return model.Book{}, err
	}

	if book.RentedUserId != userId {
		return model.Book{}, fmt.Errorf("this book was not rented by user with id %d", userId)
	}

	book.RentedUserId = 0
	if err = b.bookStorage.UpdateBook(ctx, book); err != nil {
		return model.Book{}, err
	}

	return book, nil
}
