package bookService

import (
	"context"
	"gitlab.com/mylib/internal/model"
)

type BookServicer interface {
	AddBook(ctx context.Context, title string, authorID int) (model.Book, error)
	ListBooks(ctx context.Context) ([]model.Book, error)
	RentBook(ctx context.Context, bookId, userId int) (model.Book, error)
	ReturnBook(ctx context.Context, bookId, userId int) (model.Book, error)
}
