package service

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/mylib/internal/model"
	"gitlab.com/mylib/internal/repository/postgres/userStorage"
)

type UserService struct {
	userStorage userStorage.UserStorager
}

func NewUserService(db *sql.DB) UserServicer {
	return &UserService{userStorage: userStorage.NewUserStorage(db)}
}

func (us *UserService) Create(ctx context.Context, name string, email string) error {
	newUser := model.User{
		Name:  name,
		Email: email,
	}

	err := us.userStorage.Create(ctx, newUser)
	return err
}

func (us *UserService) GetByID(ctx context.Context, id string) (model.User, error) {
	user, err := us.userStorage.GetByID(ctx, id)
	if err != nil {
		return model.User{}, err
	}
	return user, nil
}

func (us *UserService) Update(ctx context.Context, id string, name string, email string) error {
	updateUser := model.User{
		Id:    id,
		Name:  name,
		Email: email,
	}

	err := us.userStorage.Update(ctx, updateUser)

	return err
}

func (us *UserService) Delete(ctx context.Context, id string) error {
	err := us.userStorage.Delete(ctx, id)

	return err
}

func (us *UserService) List(ctx context.Context, limit string, offset string) ([]model.User, error) {
	c := model.Conditions{
		Limit:  limit,
		Offset: offset,
	}

	users, err := us.userStorage.List(ctx, c)
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (us *UserService) GetBook(ctx context.Context, userId string, bookId int) (model.User, error) {
	user, err := us.userStorage.GetByID(ctx, userId)
	if err != nil {
		return model.User{}, err
	}
	user.BooksId = append(user.BooksId, bookId)

	err = us.userStorage.Update(ctx, user)
	if err != nil {
		return model.User{}, err
	}

	return user, nil
}

func (us *UserService) ReturnBook(ctx context.Context, userId string, bookId int) (model.User, error) {
	user, err := us.userStorage.GetByID(ctx, userId)
	if err != nil {
		return model.User{}, err
	}
	if user.BooksId == nil {
		return model.User{}, fmt.Errorf("error: user with id %s have alredy empty list of books", userId)
	}

	for i, val := range user.BooksId {
		if val == bookId {
			user.BooksId = append(user.BooksId[:i], user.BooksId[i+1:]...)
			return user, nil
		}
	}

	err = us.userStorage.Update(ctx, user)
	if err != nil {
		return model.User{}, err
	}

	return model.User{}, fmt.Errorf("error: bookId %d not found on user %s", bookId, userId)
}
