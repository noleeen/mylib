package service

import (
	"context"
	"gitlab.com/mylib/internal/model"
)

type UserServicer interface {
	Create(ctx context.Context, name string, email string) error
	GetByID(ctx context.Context, id string) (model.User, error)
	Update(ctx context.Context, id string, name string, email string) error
	Delete(ctx context.Context, id string) error
	List(ctx context.Context, limit string, offset string) ([]model.User, error)
	GetBook(ctx context.Context, userId string, bookId int) (model.User, error)
	ReturnBook(ctx context.Context, userId string, bookId int) (model.User, error)
}
