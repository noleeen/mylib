package controller

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-chi/chi/v5"
	"gitlab.com/mylib/internal/moduls/user/service"
	"gitlab.com/mylib/pkg/responder"
	"net/http"
)

type UserController struct {
	userService service.UserServicer
	responder   responder.Responder
}

func NewUserController(userService service.UserServicer, responder responder.Responder) *UserController {
	return &UserController{
		userService: userService,
		responder:   responder,
	}
}

func (uc *UserController) CreateUser(w http.ResponseWriter, r *http.Request) {
	var param CreateUserRequest

	if err := json.NewDecoder(r.Body).Decode(&param); err != nil {
		uc.responder.ErrorBadRequest(w, err)
		return
	}

	err := uc.userService.Create(r.Context(), param.Name, param.Email)
	if err != nil {
		uc.responder.ErrorInternal(w, err)
		return
	}

	uc.responder.OutputJSON(w, fmt.Sprintf("new user with name: %s email: %s created successfully", param.Name, param.Email))
}

func (uc *UserController) GetByIDUser(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		uc.responder.ErrorBadRequest(w, errors.New("error: id is empty"))
		return
	}

	user, err := uc.userService.GetByID(r.Context(), id)
	if err != nil {
		uc.responder.ErrorInternal(w, err)
		return
	}

	uc.responder.OutputJSON(w, user)
}

func (uc *UserController) UpdateUser(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		uc.responder.ErrorBadRequest(w, errors.New("error: id is empty"))
		return
	}

	var param CreateUserRequest

	if err := json.NewDecoder(r.Body).Decode(&param); err != nil {
		uc.responder.ErrorBadRequest(w, err)
		return
	}

	if err := uc.userService.Update(r.Context(), id, param.Name, param.Email); err != nil {
		uc.responder.ErrorInternal(w, err)
		return
	}
	uc.responder.OutputJSON(w, fmt.Sprintf("user with id: %s updated successfully", id))
}

func (uc *UserController) DeleteUser(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		uc.responder.ErrorBadRequest(w, errors.New("error: id is empty"))
		return
	}

	err := uc.userService.Delete(r.Context(), id)
	if err != nil {
		uc.responder.ErrorInternal(w, err)
		return
	}

	uc.responder.OutputJSON(w, fmt.Sprintf("user with id: %s marked for deletion", id))
}

func (uc *UserController) ListUsers(w http.ResponseWriter, r *http.Request) {
	limit := chi.URLParam(r, "limit")
	offset := chi.URLParam(r, "offset")

	users, err := uc.userService.List(r.Context(), limit, offset)
	if err != nil {
		uc.responder.ErrorInternal(w, err)
		return
	}

	uc.responder.OutputJSON(w, users)
}
