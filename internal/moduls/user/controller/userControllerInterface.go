package controller

import "net/http"

type UserControllerer interface {
	CreateUser(w http.ResponseWriter, r *http.Request)
	GetByIDUser(w http.ResponseWriter, r *http.Request)
	UpdateUser(w http.ResponseWriter, r *http.Request)
	DeleteUser(w http.ResponseWriter, r *http.Request)
	ListUsers(w http.ResponseWriter, r *http.Request)
}
