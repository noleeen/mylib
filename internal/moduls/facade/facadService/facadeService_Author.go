package facadService

import (
	"context"
	"gitlab.com/mylib/internal/model"
)

func (f *FacadeService) AuthorAdd(ctx context.Context, name string) (model.Author, error) {
	author, err := f.AuthorService.AddAuthor(ctx, name)
	if err != nil {
		return model.Author{}, err
	}
	return author, nil
}

func (f *FacadeService) AuthorList(ctx context.Context) ([]model.Author, error) {
	authors, err := f.AuthorService.ListAuthors(ctx)
	if err != nil {
		return nil, err
	}
	return authors, nil
}

func (f *FacadeService) TopReadingAuthors(ctx context.Context, limit int) ([]model.Author, error) {
	authors, err := f.AuthorService.TopReadAuthors(ctx, limit)
	if err != nil {
		return nil, err
	}
	return authors, nil
}

func (f *FacadeService) TopAuthors(ctx context.Context, limit int) ([]model.Author, error) {
	authors, err := f.AuthorService.TopAuthorsInTheHistory(ctx, limit)
	if err != nil {
		return nil, err
	}
	return authors, nil
}
