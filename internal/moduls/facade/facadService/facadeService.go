package facadService

import (
	"database/sql"
	"gitlab.com/mylib/internal/moduls/author/authorService"
	"gitlab.com/mylib/internal/moduls/book/bookService"
	"gitlab.com/mylib/internal/moduls/user/service"
	"sync"
)

type FacadeService struct {
	BookService   bookService.BookServicer
	AuthorService authorService.AuthorServicer
	UserService   service.UserServicer
	sync.Mutex
}

func NewFacadeService(db *sql.DB) FacadeServicer {
	return &FacadeService{
		BookService:   bookService.NewBookService(db),
		AuthorService: authorService.NewAuthorService(db),
		UserService:   service.NewUserService(db),
	}
}
