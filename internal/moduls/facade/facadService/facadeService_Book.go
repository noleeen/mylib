package facadService

import (
	"context"
	"gitlab.com/mylib/internal/model"
)

func (f *FacadeService) BookAdd(ctx context.Context, title string, authorId int) (model.Book, error) {
	book, err := f.BookService.AddBook(ctx, title, authorId)
	if err != nil {
		return model.Book{}, err
	}
	return book, nil
}
func (f *FacadeService) BookList(ctx context.Context) ([]model.Book, error) {
	books, err := f.BookService.ListBooks(ctx)
	if err != nil {
		return nil, err
	}
	return books, nil
}
