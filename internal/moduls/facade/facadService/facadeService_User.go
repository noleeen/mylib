package facadService

import (
	"context"
	"gitlab.com/mylib/internal/model"
	"strconv"
)

func (f *FacadeService) RentBookByUser(ctx context.Context, bookId, userId int) (model.User, model.Book, error) {
	f.Mutex.Lock()
	defer f.Unlock()

	userIdString := strconv.Itoa(userId)
	user, err := f.UserService.GetBook(ctx, userIdString, bookId)
	if err != nil {
		return model.User{}, model.Book{}, err
	}

	book, err := f.BookService.RentBook(ctx, bookId, userId)
	if err != nil {
		return model.User{}, model.Book{}, err
	}

	_, err = f.AuthorService.AddReadCount(ctx, book.AuthorId)
	if err != nil {
		return model.User{}, model.Book{}, err
	}

	return user, book, nil
}

func (f *FacadeService) ReturnBook(ctx context.Context, bookId, userId int) (model.User, model.Book, error) {
	f.Lock()
	defer f.Unlock()

	userIdString := strconv.Itoa(userId)

	user, err := f.UserService.ReturnBook(ctx, userIdString, bookId)
	if err != nil {
		return model.User{}, model.Book{}, err
	}

	book, err := f.BookService.ReturnBook(ctx, bookId, userId)
	if err != nil {
		return model.User{}, model.Book{}, err
	}

	return user, book, nil

}

func (f *FacadeService) UserAdd(ctx context.Context, name, email string) error {
	err := f.UserService.Create(ctx, name, email)
	return err
}

func (f *FacadeService) UserList(ctx context.Context, limit, offset string) ([]model.User, error) {
	users, err := f.UserService.List(ctx, limit, offset)
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (f *FacadeService) UserById(ctx context.Context, id string) (model.User, error) {
	user, err := f.UserService.GetByID(ctx, id)
	return user, err
}
