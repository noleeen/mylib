package facadService

import (
	"context"
	"gitlab.com/mylib/internal/model"
)

type FacadeServicer interface {
	RentBookByUser(ctx context.Context, bookId, userId int) (model.User, model.Book, error)
	ReturnBook(ctx context.Context, bookId, userId int) (model.User, model.Book, error)
	UserAdd(ctx context.Context, name, email string) error
	UserById(ctx context.Context, id string) (model.User, error)
	UserList(ctx context.Context, limit, offset string) ([]model.User, error)

	AuthorAdd(ctx context.Context, name string) (model.Author, error)
	AuthorList(ctx context.Context) ([]model.Author, error)
	TopReadingAuthors(ctx context.Context, limit int) ([]model.Author, error)
	TopAuthors(ctx context.Context, limit int) ([]model.Author, error)

	BookAdd(ctx context.Context, title string, authorId int) (model.Book, error)
	BookList(ctx context.Context) ([]model.Book, error)
}
