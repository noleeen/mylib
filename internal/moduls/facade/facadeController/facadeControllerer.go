package facadeController

import "net/http"

type FacadeControllerer interface {
	//--user--
	RentBookByUser(w http.ResponseWriter, r *http.Request)
	ReturnBook(w http.ResponseWriter, r *http.Request)
	UserAdd(w http.ResponseWriter, r *http.Request)
	UserList(w http.ResponseWriter, r *http.Request)
	UserByID(w http.ResponseWriter, r *http.Request)

	//--author--
	AuthorAdd(w http.ResponseWriter, r *http.Request)
	AuthorList(w http.ResponseWriter, r *http.Request)
	TopReadingAuthors(w http.ResponseWriter, r *http.Request)
	TopAuthors(w http.ResponseWriter, r *http.Request)

	//--book--
	BookAdd(w http.ResponseWriter, r *http.Request)
	BookList(w http.ResponseWriter, r *http.Request)
}
