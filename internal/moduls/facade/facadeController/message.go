package facadeController

type CreateUserRequest struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

type CreateAuthorRequest struct {
	Name string `json:"name"`
}

type CreateBookRequest struct {
	Title    string `json:"title"`
	AuthorId int    `json:"author_id"`
}

type RentBookRequest struct {
	UserID int `json:"userID"`
	BookID int `json:"bookID"`
}
