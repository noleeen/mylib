package facadeController

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-chi/chi/v5"
	"gitlab.com/mylib/internal/moduls/facade/facadService"
	"gitlab.com/mylib/pkg/responder"
	"net/http"
	"strconv"
)

type FacadeController struct {
	FacadeService facadService.FacadeServicer
	respond       responder.Responder
}

func NewFacadeController(facadeService facadService.FacadeServicer, respond responder.Responder) FacadeControllerer {
	return &FacadeController{
		FacadeService: facadeService,
		respond:       respond,
	}
}

// ------------user-------------------
func (fc *FacadeController) RentBookByUser(w http.ResponseWriter, r *http.Request) {
	bookID := chi.URLParam(r, "bookID")
	userID := chi.URLParam(r, "userID")

	bookIdInt, err := strconv.Atoi(bookID)
	userIdInt, err := strconv.Atoi(userID)

	_, _, err = fc.FacadeService.RentBookByUser(r.Context(), bookIdInt, userIdInt)
	if err != nil {
		fc.respond.ErrorInternal(w, err)
		return
	}

	fc.respond.OutputJSON(w, fmt.Sprintf("user: %d rented book: %d", userIdInt, bookIdInt))
}

func (fc *FacadeController) ReturnBook(w http.ResponseWriter, r *http.Request) {
	bookID := chi.URLParam(r, "bookID")
	userID := chi.URLParam(r, "userID")

	bookIdInt, err := strconv.Atoi(bookID)
	userIdInt, err := strconv.Atoi(userID)

	_, _, err = fc.FacadeService.ReturnBook(r.Context(), bookIdInt, userIdInt)
	if err != nil {
		fc.respond.ErrorInternal(w, err)
		return
	}

	fc.respond.OutputJSON(w, fmt.Sprintf("user: %d returned book: %d", userIdInt, bookIdInt))
}

func (fc *FacadeController) UserAdd(w http.ResponseWriter, r *http.Request) {
	var param CreateUserRequest

	if err := json.NewDecoder(r.Body).Decode(&param); err != nil {
		fc.respond.ErrorBadRequest(w, err)
		return
	}

	err := fc.FacadeService.UserAdd(r.Context(), param.Name, param.Email)
	if err != nil {
		fc.respond.ErrorInternal(w, err)
		return
	}

	fc.respond.OutputJSON(w, fmt.Sprintf("new user with name: %s email: %s created successfully", param.Name, param.Email))
}

func (fc *FacadeController) UserList(w http.ResponseWriter, r *http.Request) {
	limit := chi.URLParam(r, "limit")
	offset := chi.URLParam(r, "offset")

	users, err := fc.FacadeService.UserList(r.Context(), limit, offset)
	if err != nil {
		fc.respond.ErrorInternal(w, err)
		return
	}

	fc.respond.OutputJSON(w, users)
}

func (fc *FacadeController) UserByID(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		fc.respond.ErrorBadRequest(w, errors.New("error: id is empty"))
		return
	}

	user, err := fc.FacadeService.UserById(r.Context(), id)
	if err != nil {
		fc.respond.ErrorInternal(w, err)
		return
	}

	fc.respond.OutputJSON(w, user)
}

// ------------author-------------------
func (fc *FacadeController) AuthorAdd(w http.ResponseWriter, r *http.Request) {
	var param CreateAuthorRequest

	if err := json.NewDecoder(r.Body).Decode(&param); err != nil {
		fc.respond.ErrorBadRequest(w, err)
		return
	}

	author, err := fc.FacadeService.AuthorAdd(r.Context(), param.Name)
	if err != nil {
		fc.respond.ErrorInternal(w, err)
		return
	}

	fc.respond.OutputJSON(w, author)

}

func (fc *FacadeController) AuthorList(w http.ResponseWriter, r *http.Request) {
	authors, err := fc.FacadeService.AuthorList(r.Context())
	if err != nil {
		fc.respond.ErrorInternal(w, err)
		return
	}

	fc.respond.OutputJSON(w, authors)
}

func (fc *FacadeController) TopReadingAuthors(w http.ResponseWriter, r *http.Request) {
	limit := chi.URLParam(r, "limit")
	limitInt, err := strconv.Atoi(limit)
	if err != nil {
		fc.respond.ErrorBadRequest(w, err)
		return
	}

	authors, err := fc.FacadeService.TopReadingAuthors(r.Context(), limitInt)
	if err != nil {
		fc.respond.ErrorInternal(w, err)
		return
	}
	fc.respond.OutputJSON(w, authors)
}

func (fc *FacadeController) TopAuthors(w http.ResponseWriter, r *http.Request) {
	limit := chi.URLParam(r, "limit")
	limitInt, err := strconv.Atoi(limit)
	if err != nil {
		fc.respond.ErrorBadRequest(w, err)
		return
	}

	authors, err := fc.FacadeService.TopAuthors(r.Context(), limitInt)
	if err != nil {
		fc.respond.ErrorInternal(w, err)
		return
	}
	fc.respond.OutputJSON(w, authors)
}

// ------------book-------------------
func (fc *FacadeController) BookAdd(w http.ResponseWriter, r *http.Request) {
	var param CreateBookRequest

	err := json.NewDecoder(r.Body).Decode(&param)
	if err != nil {
		fc.respond.ErrorBadRequest(w, err)
		return
	}

	book, err := fc.FacadeService.BookAdd(r.Context(), param.Title, param.AuthorId)
	if err != nil {
		fc.respond.ErrorInternal(w, err)
		return
	}
	fc.respond.OutputJSON(w, book)
}

func (fc *FacadeController) BookList(w http.ResponseWriter, r *http.Request) {
	books, err := fc.FacadeService.BookList(r.Context())
	if err != nil {
		fc.respond.ErrorInternal(w, err)
		return
	}

	fc.respond.OutputJSON(w, books)
}
