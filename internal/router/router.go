package router

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/mylib/internal/moduls/facade/facadeController"
	"gitlab.com/mylib/static"
	"net/http"
)

func NewApiRouter(fc facadeController.FacadeControllerer) http.Handler {
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	r.Route("/author", func(r chi.Router) {
		r.Post("/add", fc.AuthorAdd)
		r.Get("/list", fc.AuthorList)
		r.Get("/reading_now/{limit}", fc.TopReadingAuthors)
		r.Get("/top/{limit}", fc.TopAuthors)
	})

	r.Route("/book", func(r chi.Router) {
		r.Post("/add", fc.BookAdd)
		r.Get("/list", fc.BookList)
	})

	r.Route("/user", func(r chi.Router) {
		r.Post("/add", fc.UserAdd)
		r.Get("/{id}", fc.UserByID)
		r.Get("/{limit}/{offset}", fc.UserList)
		r.Put("/take/{userID}/{bookID}", fc.RentBookByUser)
		r.Put("/return/{userID}/{bookID}", fc.ReturnBook)
	})

	//r.Route("/user", func(r chi.Router) {
	//	r.Post("/", uc.CreateUser)
	//	r.Get("/{id}", uc.GetByIDUser)
	//	r.Put("/{id}", uc.UpdateUser)
	//	r.Delete("/{id}", uc.DeleteUser)
	//	r.Get("/{limit}/{offset}", uc.ListUsers)
	//
	//})

	//swagger
	r.Get("/swagger", static.SwaggerUI)
	r.Get("/static/swagger.json", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))).ServeHTTP(w, r)
	})
	return r
}
