package model

import "time"

//go:generate easytags $GOFILE json,db
type User struct {
	Id      string     `json:"id" db:"id"`
	Name    string     `json:"name" db:"name"`
	Email   string     `json:"email" db:"email"`
	BooksId []int      `json:"books" db:"books"`
	Delete  *time.Time `json:"delete,omitempty" db:"deleted"`
}

type Conditions struct {
	Limit  string `json:"limit" db:"limit"`
	Offset string `json:"offset" db:"offset"`
}
