package model

//go:generate easytags $GOFILE json,db
type Book struct {
	ID           int    `db:"id" json:"id"`
	Title        string `db:"title" json:"title"`
	AuthorId     int    `db:"author_id" json:"author_id,omitempty"`
	RentedUserId int    `db:"rented_user_id" json:"rented_user_id"`
	Author       `db:"author" json:"author,omitempty"`
}
