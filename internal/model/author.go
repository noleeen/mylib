package model

//go:generate easytags $GOFILE json,db
type Author struct {
	ID           int    `db:"id" json:"id,omitempty"`
	Name         string `db:"name" json:"name,omitempty"`
	Books        []Book `db:"book" json:"books,omitempty"`
	CountReading int    `db:"count_reading" json:"read,omitempty"`
}
