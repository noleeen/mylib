package userStorage

import (
	"context"
	"gitlab.com/mylib/internal/model"
)

type UserStorager interface {
	Create(ctx context.Context, user model.User) error
	GetByID(ctx context.Context, id string) (model.User, error)
	Update(ctx context.Context, user model.User) error
	Delete(ctx context.Context, id string) error
	List(ctx context.Context, c model.Conditions) ([]model.User, error)
}
