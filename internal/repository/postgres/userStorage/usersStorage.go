package userStorage

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/lib/pq"
	"gitlab.com/mylib/internal/model"
	"time"
)

type userStorage struct {
	db *sql.DB
}

func NewUserStorage(db *sql.DB) UserStorager {
	return &userStorage{db: db}
}

func (u *userStorage) Create(ctx context.Context, user model.User) error {
	stmt, err := u.db.Prepare("insert into users (name,email) values ($1,$2)")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, user.Name, user.Email)
	return err
}

func (u *userStorage) GetByID(ctx context.Context, id string) (model.User, error) {
	stmt, err := u.db.Prepare("SELECT id, name, email, COALESCE(books_id, '{}'), deleted FROM users WHERE id = $1")
	if err != nil {
		return model.User{}, err
	}
	defer stmt.Close()

	user := model.User{}

	var x []sql.NullInt64
	err = stmt.QueryRowContext(ctx, id).Scan(&user.Id, &user.Name, &user.Email, pq.Array(&x), &user.Delete)

	for _, val := range x {
		user.BooksId = append(user.BooksId, int(val.Int64))
	}
	return user, err
}

func (u *userStorage) Update(ctx context.Context, user model.User) error {
	stmt, err := u.db.Prepare("update users set name=$1, email=$2, books_id=$3 where id = $4")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, user.Name, user.Email, pq.Array(user.BooksId), user.Id)

	fmt.Println("ПРОВЕРКА update. user:", user)

	return err
}

func (u *userStorage) Delete(ctx context.Context, id string) error {
	stmt, err := u.db.Prepare("update users set deleted = $1 where id = $2")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, time.Now().Format("2006-01-02T15:04:05"), id)

	return err
}

func (u *userStorage) List(ctx context.Context, c model.Conditions) ([]model.User, error) {
	stmt, err := u.db.Prepare(`
		select id, name, email, COALESCE(books_id, '{}') as books_id, deleted 
		from users where deleted is null limit $1 offset $2
		`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, c.Limit, c.Offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var users []model.User
	for rows.Next() {
		user := model.User{}
		var booksId []sql.NullInt64

		if err = rows.Scan(&user.Id, &user.Name, &user.Email, pq.Array(&booksId), &user.Delete); err != nil {
			return nil, err
		}

		for _, id := range booksId {
			user.BooksId = append(user.BooksId, int(id.Int64))
		}

		users = append(users, user)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return users, nil
}
