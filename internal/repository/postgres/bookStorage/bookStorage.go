package bookStorage

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/mylib/internal/model"
	"log"
)

type BookStorage struct {
	db *sql.DB
}

func NewBookStorage(db *sql.DB) BookStorager {
	return &BookStorage{db: db}
}

func (b *BookStorage) AddBook(ctx context.Context, book model.Book) (model.Book, error) {
	stmt, err := b.db.Prepare("insert into books (title,author_id) values ($1,$2) RETURNING id")
	if err != nil {
		return model.Book{}, err
	}
	defer stmt.Close()

	err = stmt.QueryRowContext(ctx, book.Title, book.AuthorId).Scan(&book.ID)
	if err != nil {
		return model.Book{}, err
	}

	return book, nil
}

func (b *BookStorage) ListBooks(ctx context.Context) ([]model.Book, error) {
	stmt, err := b.db.Prepare(`
		SELECT books.id id, title, coalesce(rented_user_id, 0), authors.id , authors.name 
		FROM books left join authors on books.author_id=authors.id
		ORDER BY books.id`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var books []model.Book
	for rows.Next() {
		book := model.Book{}
		if err = rows.Scan(&book.ID, &book.Title, &book.RentedUserId, &book.AuthorId, &book.Author.Name); err != nil {
			log.Println(err)
			fmt.Println(err)
			continue
		}
		books = append(books, book)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return books, nil

}

func (b *BookStorage) UpdateBook(ctx context.Context, book model.Book) error {

	stmt, err := b.db.Prepare(`
		UPDATE books 
		SET title=$1, author_id=$2, rented_user_id=$3 WHERE id=$4`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	//если пользователь вернул книгу нужно в базу записать nil, так как, если будет 0, то база
	//будет выкидывать ошибку, что пользователя 0 нету, а этот столбец связан с пользователями
	if book.RentedUserId == 0 {
		var temp *int
		//temp = nil
		_, err = stmt.ExecContext(ctx, book.Title, book.AuthorId, temp, book.ID)
	} else {

		_, err = stmt.ExecContext(ctx, book.Title, book.AuthorId, book.RentedUserId, book.ID)
	}

	return err
}

func (b *BookStorage) GetBook(ctx context.Context, id int) (model.Book, error) {
	stmt, err := b.db.Prepare(`
		SELECT id, title, author_id, coalesce(rented_user_id, 0) rented_user_id 
		FROM books 
		WHERE id=$1`)
	if err != nil {
		return model.Book{}, err
	}
	var book model.Book
	err = stmt.QueryRowContext(ctx, id).Scan(&book.ID, &book.Title, &book.AuthorId, &book.RentedUserId)
	if err != nil {
		return model.Book{}, err
	}

	return book, nil
}
