package bookStorage

import (
	"context"
	"gitlab.com/mylib/internal/model"
)

type BookStorager interface {
	AddBook(ctx context.Context, book model.Book) (model.Book, error)
	ListBooks(ctx context.Context) ([]model.Book, error)
	UpdateBook(ctx context.Context, book model.Book) error
	GetBook(ctx context.Context, id int) (model.Book, error)
}
