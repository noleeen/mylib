package authorStorage

import (
	"context"
	"gitlab.com/mylib/internal/model"
)

type AuthorStorager interface {
	AddAuthor(ctx context.Context, author model.Author) (model.Author, error)
	ListsAuthor(ctx context.Context) ([]model.Author, error)
	TopReadingAuthors(ctx context.Context, limit int) ([]model.Author, error)
	AddReadCount(ctx context.Context, authorId int) (model.Author, error)
	TopAuthorsInTheHistory(ctx context.Context, limit int) ([]model.Author, error)
}
