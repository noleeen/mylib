package authorStorage

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/mylib/internal/model"
	"log"
)

type AuthorStorage struct {
	db *sql.DB
}

func NewAuthorStorage(db *sql.DB) AuthorStorager {
	return &AuthorStorage{db: db}
}

func (a *AuthorStorage) AddAuthor(ctx context.Context, author model.Author) (model.Author, error) {
	stmt, err := a.db.Prepare("insert into authors (name,count_reading) values ($1,$2) RETURNING id")
	if err != nil {
		return model.Author{}, err
	}
	defer stmt.Close()

	err = stmt.QueryRowContext(ctx, author.Name, 0).Scan(&author.ID)
	if err != nil {
		return model.Author{}, err
	}

	return author, nil
}

func (a *AuthorStorage) ListsAuthor(ctx context.Context) ([]model.Author, error) {
	stmt, err := a.db.Prepare(`
		select a.id, a.name, coalesce(b.id, 0), coalesce(b.title, ''), coalesce(b.rented_user_id,0)
		from authors a
		left join books b on a.id = b.author_id
		order by a.id, b.id`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var authors []model.Author
	currentID := 0
	for rows.Next() {
		author := model.Author{}
		bookID := 0
		bookTitle := ""
		bookRented := 0
		if err = rows.Scan(&author.ID, &author.Name, &bookID, &bookTitle, &bookRented); err != nil {
			log.Println(err)
			fmt.Println(err)
			continue
		}
		if author.ID != currentID {
			currentID = author.ID
			author.Books = nil
			if bookID != 0 {
				author.Books = append(author.Books, model.Book{ID: bookID, Title: bookTitle, RentedUserId: bookRented})
			}
			authors = append(authors, author)
		} else if bookID != 0 {
			authors[len(authors)-1].Books = append(
				authors[len(authors)-1].Books,
				model.Book{ID: bookID, Title: bookTitle, RentedUserId: bookRented},
			)
		}
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return authors, nil
}

func (a *AuthorStorage) TopReadingAuthors(ctx context.Context, limit int) ([]model.Author, error) {
	stmt, err := a.db.Prepare(`
		select a.id, a.name rented_books, count(b.rented_user_id) read
		from books b join authors a on b.author_id = a.id
		group by a.id, a.name
		order by read desc limit $1`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, limit)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var authors []model.Author
	for rows.Next() {
		author := model.Author{}
		if err = rows.Scan(&author.ID, &author.Name, &author.CountReading); err != nil {
			log.Println(err)
			fmt.Println(err)
			continue
		}
		authors = append(authors, author)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return authors, nil
}

func (a *AuthorStorage) AddReadCount(ctx context.Context, authorId int) (model.Author, error) {
	stmt, err := a.db.Prepare(`
		UPDATE authors 
		SET count_reading = count_reading + 1 
		WHERE id=$1
		RETURNING id, name, count_reading`)
	if err != nil {
		return model.Author{}, err
	}
	defer stmt.Close()

	var author model.Author
	err = stmt.QueryRowContext(ctx, authorId).Scan(&author.ID, &author.Name, &author.CountReading)
	fmt.Println("ПРОВЕРКА", author, "count reading:", author.CountReading)
	return author, err
}

func (a *AuthorStorage) TopAuthorsInTheHistory(ctx context.Context, limit int) ([]model.Author, error) {

	query := `
        SELECT id, name, count_reading
        FROM authors
        ORDER BY count_reading DESC
        LIMIT $1
    `

	rows, err := a.db.QueryContext(ctx, query, limit)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var authors []model.Author
	for rows.Next() {
		var author model.Author
		if err = rows.Scan(&author.ID, &author.Name, &author.CountReading); err != nil {
			return nil, err
		}

		fmt.Println(author.Name, author.CountReading)
		authors = append(authors, author)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}

	return authors, nil
}
