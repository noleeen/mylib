package postgres

import (
	"database/sql"
	"gitlab.com/mylib/internal/repository/postgres/userStorage"
)

type StoragePostgres struct {
	DB          *sql.DB
	UserStorage userStorage.UserStorager
}

func NewStoragePostgres(db *sql.DB) *StoragePostgres {

	return &StoragePostgres{
		DB:          db,
		UserStorage: userStorage.NewUserStorage(db),
	}
}

func (s *StoragePostgres) CreateTables() error {
	_, err := s.DB.Exec(`
		CREATE TABLE IF NOT EXISTS users(
		    id SERIAL PRIMARY KEY,
		    name VARCHAR(40),
		    email TEXT,
		    books_id INTEGER[],
		    deleted TIMESTAMP
		);
	`)

	_, err = s.DB.Exec(`
		CREATE TABLE IF NOT EXISTS authors(
		    id SERIAL PRIMARY KEY,
		    name VARCHAR(100),
		    count_reading INTEGER 
		);
	`)

	_, err = s.DB.Exec(`
		CREATE TABLE IF NOT EXISTS books(
		    id SERIAL PRIMARY KEY,
		    title VARCHAR(255),
		    author_id INTEGER NOT NULL REFERENCES authors,
		    rented_user_id INTEGER REFERENCES users
		);
	`)

	return err
}
