FROM golang:1.19.11-alpine as builder

WORKDIR /app

COPY . .


# Собираем приложение
RUN go build -o main cmd/main.go


# Начинаем новую стадию сборки на основе минимального образа
FROM alpine:latest

# Добавляем исполняемый файл из первой стадии в корневую директорию контейнера
COPY --from=builder /app/main /main
COPY --from=builder /app/static /static
COPY --from=builder /app/.env /.env

# Открываем порт 8080
EXPOSE 8080

# Запускаем приложение
CMD ["/main"]
