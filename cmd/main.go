package main

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"gitlab.com/mylib/config"
	"gitlab.com/mylib/internal/moduls/facade/facadService"
	facadeController2 "gitlab.com/mylib/internal/moduls/facade/facadeController"
	"gitlab.com/mylib/internal/repository/postgres"
	"gitlab.com/mylib/internal/router"
	"gitlab.com/mylib/pkg/responder"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

func main() {
	if err := godotenv.Load(); err != nil {
		log.Fatalf("Error loading .env file: %v", err)
	}
	cfg := config.NewConfig()
	dsn := cfg.GetDsnDB()

	fmt.Println("dsn:", dsn)
	//"host=127.0.0.1 port=5432 user=postgres dbname=postgres sslmode=disable password=secret"

	db, err := sql.Open(cfg.DB.Driver, dsn)
	if err != nil {
		fmt.Println("error sql.Open")
		log.Fatal(err)
	}

	//if err = db.Ping(); err != nil {
	//	fmt.Println("error db.Ping", err)
	//	log.Fatal(err)
	//}
	//fmt.Println("CONNECTED")

	storages := postgres.NewStoragePostgres(db)

	err = storages.CreateTables()
	if err != nil {
		log.Fatal(err)
	}

	resp := responder.NewResponder()
	facadeService := facadService.NewFacadeService(db)
	facadeController := facadeController2.NewFacadeController(facadeService, resp)

	r := router.NewApiRouter(facadeController)
	fmt.Println("router:", r)

	server := &http.Server{
		Addr:    ":8080",
		Handler: r,
	}

	//-------------------------------------------------------------
	////создаем канал для принятия сигналов о завершении работы
	//signChan := make(chan os.Signal, 1)
	////функция signal.Notify для регистрации обработчика сигналов
	//signal.Notify(signChan, syscall.SIGINT, syscall.SIGTERM)
	//
	////запускаем сервер в горутине
	//go func() {
	//	fmt.Println("server is running on :8080")
	//	if err = server.ListenAndServe(); err != nil {
	//		log.Fatalf("server error: %v", err)
	//	}
	//
	//}()
	//
	////ожидаем сигнал завершения работы
	//<-signChan
	//fmt.Println("Shutting down server...")
	//
	//// Ожидаем завершения всех активных запросов в течение 5 секунд
	//ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	//defer cancel()
	//
	//if err = server.Shutdown(ctx); err != nil {
	//	fmt.Printf("Error during server shutdown: %v\n", err)
	//}
	//
	//fmt.Println("Server gracefully stopped")
	//------------------------------------------------------------------------

	//------- gracefulShutdown_2 ----------------------------------------------------------------

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		err = server.ListenAndServe()
		if err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatalf("Server error: %v", err)
		}
	}()
	wg := sync.WaitGroup{}
	go func() {
		wg.Add(1)
		server.RegisterOnShutdown(func() {
			// server.RegisterOnShutdown и server.Shutdown должны успеть завершиться за общее время
			log.Println("можем тут что-нибудь сделать до отключения сервера")
			wg.Done()
		})
	}()

	go func() {
		log.Printf("PID:%v", os.Getpid())
	}()
	select {
	case <-signalChan:
		ctx, cancelFunc := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancelFunc()
		err := server.Shutdown(ctx)
		if err != nil {
			log.Fatal(err)
		}
	}
	wg.Wait()
	fmt.Println("Server stopped gracefully")

	//-------------------------------------------------------------------------------------------

}
