package static

import "gitlab.com/mylib/internal/model"

// swagger:route POST /book/add Books CreateBookRequest
// Create new book.
// responses:
// 200: CreateBookResponse
// 400: description: invalid request
// 500: description: error create book

// swagger:parameters CreateBookRequest
type CreateBookRequest struct {
	// in:body
	// Required: true
	// Example: {"title": "The War", "author_id": 1}
	Body struct {
		// Title of the book.
		// Example: Vasya
		// required: true
		Title string `json:"title"`

		// Author's ID.
		// Example: 2
		// required: true
		AuthorID int `json:"author_id"`
	}
}

// swagger:response CreateBookResponse
type CreateBookResponse struct {
	//in:body
	Body model.Book
}

// swagger:route GET /book/list Books ListBookRequest
//	Get books list.
// responses:
// 200: ListBookResponse
// 400: description: Bad Request
// 500: description: Internal Error

// swagger:parameters ListBookRequest
type ListBookRequest struct {
	// in:path
	// required:true
}

// swagger:response ListBookResponse
type ListBookResponse struct {
	// in: body
	Body []model.Book
}
