// Package static Users.
//
//	Documentation API.
//
//		Version: 1.0.0
//		Schemes: http, https
//		BasePath: /
//
//		Consumes:
//		- application/json
//	 	- multipart/form-data
//
//		Produces:
//		- application/json
//
//		Security:
//		- basic
//
// swagger:meta
package static

//go:generate swagger generate spec -o ./swagger.json --scan-models
