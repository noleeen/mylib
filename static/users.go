package static

import "gitlab.com/mylib/internal/model"

// swagger:route POST /user/add Users CreateUserRequest
// Create new user.
// responses:
// 200: CreateUserResponse
// 400: description: invalid request
// 500: description: error create user

// swagger:parameters CreateUserRequest
type CreateUserRequest struct {
	// in:body
	// Required: true
	// Example: {"name": "Vasya", "email": "v@minsk.by"}
	Body struct {
		// Name of the user.
		// Example: Vasya
		// required: true
		Name string `json:"name"`

		// Email of the user.
		// Example: v@minsk.by
		// required: true
		Email string `json:"email"`
	}
}

// swagger:response CreateUserResponse
type CreateUserResponse struct {
	//in:body
	Body string
}

// swagger:route GET /user/{id} Users GetUserByIDRequest
// Get user by ID.
// responses:
//   200: GetUserByIDResponse
//   400: description: User ID is required.
//   500: description: Error fetching user.

// swagger:parameters GetUserByIDRequest
type GetUserByIDRequest struct {
	//in:path
	// required: true
	UserID string `json:"id"`
}

// swagger:response GetUserByIDResponse
type GetUserByIDResponse struct {
	// in:body
	Body model.User
}

// swagger:route PUT /user/{id} Users UpdateUserRequest
// Update user.
// responses:
//   200: UpdateUserResponse
//   400: description: Invalid request.
//   500: description: Error fetching user.

// swagger:parameters UpdateUserRequest
type UpdateUserRequest struct {
	//in:path
	// required: true
	ID string `json:"id"`
	//in:body
	// Example: {"name": "Vasya", "email": "v@minsk.by"}
	Body struct {
		// Name of the user.
		// required: true
		Name string `json:"name"`

		// Email of the user.
		// required: true
		Email string `json:"email"`
	}
}

// swagger:response UpdateUserResponse
type UpdateUserResponse struct {
	// in:body
	Body string
}

// swagger:route DELETE /user/{id} Users DeleteUserRequest
// Delete user.
// responses:
//   200: DeleteUserResponse
//   400: description: User ID is required
//   500: description: Failed to delete user.

// swagger:parameters DeleteUserRequest
type DeleteUserRequest struct {
	//in:path
	// required: true
	Id string `json:"id"`
}

// swagger:response DeleteUserResponse
type DeleteUserResponse struct {
	// in:body
	Body string
}

// swagger:route GET /user/{limit}/{offset} Users ListUsersRequest
// List Users.
// responses:
//   200: ListUsersResponse
//   400: description: Error request
//   500: description: Failed to list users.

// swagger:parameters ListUsersRequest
type ListUsersRequest struct {
	// in: path
	// required: true
	// example:  "limit" = 10
	Limit string `json:"limit"`
	// in: path
	// required: true
	// example:  "offset" = 1
	Offset string `json:"offset"`
}

// swagger:response ListUsersResponse
type ListUsersResponse struct {
	// in:body
	Body []model.User
}

// swagger:route PUT /user/take/{userID}/{bookID} Users TakeBookRequest
// Book taking.
// responses:
//   200: TakeBookResponse
//   400: description: Error request
//   500:

// swagger:parameters TakeBookRequest
type TakeBookRequest struct {
	// in: path
	// required: true
	UserID int `json:"userID"`
	// in: path
	// required: true
	BookID int `json:"bookID"`
}

// swagger:response TakeBookResponse
type TakeBookResponse struct {
	// in:body
	Body string
}

// swagger:route PUT /user/return/{userID}/{bookID} Users ReturnBookRequest
// Book return.
// responses:
//   200: ReturnBookResponse
//   400: description: Error request
//   500:

// swagger:parameters ReturnBookRequest
type ReturnBookRequest struct {
	// in: path
	// required: true
	UserID string `json:"userID"`
	// in: path
	// required: true
	BookID string `json:"bookID"`
}

// swagger:response ReturnBookResponse
type ReturnBookResponse struct {
	// in:body
	Body string
}
