package static

import "gitlab.com/mylib/internal/model"

// swagger:route POST /author/add Authors CreateAuthorRequest
//	Create new author.
// responses:
//	 200: CreateAuthorResponse
//   400: description: Bad Request
//   500: description: Internal Error

// swagger:parameters CreateAuthorRequest
type CreateAuthorRequest struct {
	// in:body
	// Required: true
	// Example: {"name": "Vasya Bondar"}
	Body struct {
		// Name of the author.
		// Example: Vasya Bondar
		// required: true
		Name string `json:"name"`
	}
}

// swagger:response CreateAuthorResponse
type CreateAuthorResponse struct {
	//in:body
	Body model.Author
}

// swagger:route GET /author/list Authors ListAuthorRequest
//	Get author lists.
// responses:
//	 200: ListAuthorResponse
//   400: description: Bad Request
//   500: description: Internal Error

// swagger:parameters ListAuthorRequest
type ListAuthorRequest struct {
	// in:path
	// required:true
}

// swagger:response ListAuthorResponse
type ListAuthorResponse struct {
	// in: body
	Body []model.Author
}

// swagger:route GET /author/reading_now/{limit} Authors TopOfReadingAuthorsRequest
// List of the most read authors now.
// responses:
//   200: TopOfReadingAuthorsResponse
//   400: description: Error request
//   500: description: Failed to list authors.

// swagger:parameters TopOfReadingAuthorsRequest
type TopOfReadingAuthorsRequest struct {
	// in: path
	// required: true
	// example:  "limit" = 10
	Limit string `json:"limit"`
}

// swagger:response TopOfReadingAuthorsResponse
type TopOfReadingAuthorsResponse struct {
	// in:body
	Body []model.Author
}

// swagger:route GET /author/top/{limit} Authors TopAuthorsRequest
// List of the top authors.
// responses:
//   200: TopAuthorsResponse
//   400: description: Error request
//   500: description: Failed to list authors.

// swagger:parameters TopAuthorsRequest
type TopAuthorsRequest struct {
	// in: path
	// required: true
	// example:  "limit" = 10
	Limit string `json:"limit"`
}

// swagger:response TopAuthorsResponse
type TopAuthorsResponse struct {
	// in:body
	Body []model.Author
}
